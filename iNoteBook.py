# iNoteBooK

import tkinter as tk
import tkinter.ttk as ttk
import iNoteBookUI as inbUI

class NoteBook():
	
	self.ver = "1.0.0"
	
	def __init__(self, argc, argv):
		# self: object itself
		# argc: len(argv)
		# argv: iNoteBook arguments
		try:
			self.root = tk.Tk()
			self.root.title("iNoteBook")
		except:
			print("ERROR: iNoteBook needs GUI!")
			exit(1)
		else:
			self.createUI()
	
	def createUI(self):
		inbUI.ui(self.root)
